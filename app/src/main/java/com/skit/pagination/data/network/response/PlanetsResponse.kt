package com.skit.pagination.data.network.response

import com.skit.pagination.data.models.Planet
import java.util.ArrayList

class PlanetsResponse {
    var count: Int? = null
    var next: String? = null
    var previous: String? = null
    var results: List<Planet>? = ArrayList()
}