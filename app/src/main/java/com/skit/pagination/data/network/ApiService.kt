package com.skit.pagination.data.network

import com.skit.pagination.data.network.response.PlanetsResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    /**
     * Retrieve a list of planets
     */
    @GET("planets/")
    fun getPlanets(@Query("page") page: Int): Call<PlanetsResponse>
}