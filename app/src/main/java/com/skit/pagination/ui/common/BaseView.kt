package com.skit.pagination.ui.common

interface BaseView {
    fun showProgress()
    fun hideProgress()
    fun showProgressLoadMore()
    fun hideProgressLoadMore()
    fun showError()
}