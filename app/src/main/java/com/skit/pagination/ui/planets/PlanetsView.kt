package com.skit.pagination.ui.planets

import com.skit.pagination.data.models.Planet
import com.skit.pagination.ui.common.BaseView

interface PlanetsView: BaseView {
    fun updatePlanetList(planets: List<Planet>, nextPage: Int?)
}