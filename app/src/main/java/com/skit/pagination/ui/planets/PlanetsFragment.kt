package com.skit.pagination.ui.planets

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.skit.pagination.R
import com.skit.pagination.data.models.Planet
import com.skit.pagination.ui.utils.InfiniteScrollListener
import kotlinx.android.synthetic.main.fragment_planets.*
import org.jetbrains.anko.support.v4.toast

class PlanetsFragment : Fragment(), PlanetsView {

    private val planetsPresenter : PlanetsPresenter = PlanetsPresenter(this, PlanetsInteractor())
    private var planets: MutableList<Planet> = ArrayList()
    private var nextPage: Int? = 1

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_planets, container, false)
    }

    override fun onStart() {
        super.onStart()
        setAdapter(rv_planets, planets)
        planetsPresenter.loadPlanets(nextPage)
    }

    override fun onDestroy() {
        super.onDestroy()
        planetsPresenter.onDestroy()
    }

    fun setAdapter(recyclerView: RecyclerView, planets: List<Planet>) {
        val linearLayoutManager = LinearLayoutManager(context)
        recyclerView.layoutManager = linearLayoutManager
        recyclerView.adapter = PlanetsAdapter(planets)
        recyclerView.addOnScrollListener(InfiniteScrollListener({ loadMore() }, linearLayoutManager))
    }

    fun loadMore() {
        nextPage?.let { planetsPresenter.loadPlanets(it) }
    }

    override fun showProgress() {
        progress_planets.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progress_planets.visibility = View.GONE
    }

    override fun showProgressLoadMore() {
        progress_planets_load_more.visibility = View.VISIBLE
    }

    override fun hideProgressLoadMore() {
        progress_planets_load_more.visibility = View.GONE
    }

    override fun updatePlanetList(planets: List<Planet>, nextPage: Int?) {
        this.planets.addAll(planets)
        rv_planets.adapter?.notifyDataSetChanged()
        this.nextPage = nextPage
    }

    override fun showError() {
        toast("Error loading planets...")
    }
}
