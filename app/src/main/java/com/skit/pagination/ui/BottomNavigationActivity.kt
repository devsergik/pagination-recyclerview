package com.skit.pagination.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.appcompat.app.AppCompatActivity
import com.skit.pagination.R
import com.skit.pagination.ui.planets.PlanetsFragment
import kotlinx.android.synthetic.main.activity_bottom_navigation.*

class BottomNavigationActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bottom_navigation)

        showFragment(PlanetsFragment())

        bottom_navigation.setOnNavigationItemSelectedListener {
            when(it.itemId) {
                R.id.action_planets -> {
                    showFragment(PlanetsFragment())
                    true
                }

                R.id.action_characters -> {
                    showFragment(PlanetsFragment())
                    true
                }

                R.id.action_starships -> {
                    showFragment(PlanetsFragment())
                    true
                }

                else -> false
            }
        }
    }

    fun showFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
                .replace(R.id.navigation_placeholder, fragment)
                .commit()
    }
}